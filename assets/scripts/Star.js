cc.Class({
    extends: cc.Component,

    properties: {
        pickRadius: 0
    },

    // Actions functions
    getPlayerDistance: function (){
        var playerPos = this.game.player.getPosition();
        var dist = this.node.position.sub(playerPos).mag();
        return dist;
    },

    onPicked: function() {
        this.game.spawnNewStar();
        //this.game.point();
        this.node.destroy();
    },

    // onLoad () {},

    start () {

    },

    update: function (dt) {
         if (this.getPlayerDistance() < this.pickRadius ){
            this.onPicked();
            return;
        }
    },
});
