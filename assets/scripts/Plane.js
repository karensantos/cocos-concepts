// Atividade 1: conceitos iniciais (key events / touch events)
/*Proximos passos: 
* - Pontuação
* - Inimigos
* - Colisão
*/


cc.Class({
    extends: cc.Component,

    properties: {
        velocity: 200,
        accelFront: false,
        accelBack: false,
        accelTop: false,
        accelBottom: false,
        jumpAudio: {
            default: null,
            type: cc.AudioClip
        }
    },

    playSound: function (){
        cc.audioEngine.playEffect(this.jumpAudio, false);
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        // Enable key up
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        //Disabel key down
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    },

    // Move plane
    onKeyDown: function (event) {
        // set a flag when key pressed
        switch(event.keyCode) {
            case cc.macro.KEY.d:
                this.accelFront = true;
                break;
            case cc.macro.KEY.a:
                this.accelBack = true;
                break;
            case cc.macro.KEY.w:
                this.accelTop = true;
                break;
            case cc.macro.KEY.s:
                this.accelBottom = true;
                break;
        }

    },

    onKeyUp (event) {
        // unset a flag when key released
        switch(event.keyCode){
            case cc.macro.KEY.d:
                this.accelFront = false;
                break;
            case cc.macro.KEY.a:
                this.accelBack = false;
                break;
            case cc.macro.KEY.w:
                this.accelTop = false;
                break;
            case cc.macro.KEY.s:
                this.accelBottom = false;
                break;
        }
    },

    start () {

    },

    update: function (dt) {
        if(this.accelFront && this.node.position.x < this.node.parent.getContentSize().width / 2){
            this.node.setPosition(this.node.position.x += 8, this.node.position.y)
        }

        if( this.accelBack ){
            this.node.setPosition(this.node.position.x -= 8, this.node.position.y)
        }

        if( this.accelTop){
            this.node.setPosition(this.node.position.x , this.node.position.y += 8)
        }

        if( this.accelBottom){
            this.node.setPosition(this.node.position.x , this.node.position.y -= 8)
        }

        this.node.parent.on('mousedown', ()=>{
            console.log("Ta no down")
            this.node.setPosition(this.node.position.x +=8, this.node.position.y)
        }, this)

        // TO-DO: bug in the move with touch in browser 
        // this.node.parent.on('touchstart', ()=>{
        //     console.log("Ta no start")
        //     this.node.setPosition(this.node.position.x +=1, this.node.position.y)
        // }, this)
        // this.node.parent.on('touchcancel', ()=>{
        //     console.log("Ta no cancel")
        //     this.node.setPosition(this.node.position.x - this.velocity * dt, this.node.position.y)
        // }, this)
    },
});
