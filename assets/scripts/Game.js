cc.Class({
    extends: cc.Component,

    properties: {
        starPrefab: {
            default: null,
            type: cc.Prefab
        },
        maxStarDuration: 0,
        minStarDuration: 0,
        player: {
            default: null,
            type: cc.Node
        },
        scoreDisplay: {
            default: null,
            type: cc.Label
        }
    },

    // Actions functions
    spawnNewStar: function (){
        var newStar = cc.instantiate(this.starPrefab);
        this.node.addChild(newStar);
        newStar.setPosition(this.getNewStarPosition());
        newStar.getComponent('Star').game = this;
    },

    getNewStarPosition: function(){
        var randX = 0;
        var randY = this.groundY + Math.random() * this.player.getContentSize().width;
        var maxX = this.node.width/2;
        randX = (Math.random() - 0.5) * 2 * maxX;
        return cc.v2(randX, randY);
    },

    point: function (){
        this.score += 1;
        this.scoreDisplay.string = 'Score: ' + this.score;
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad: function () {
        this.spawnNewStar();
        this.score = 0;
    },

    start () {

    },

    // update (dt) {},
});
