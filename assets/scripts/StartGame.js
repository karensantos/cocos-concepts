cc.Class({
    extends: cc.Component,

    properties: {
    
    },

    playGame:function (touch, event){
        console.log(">> Play the game")
        cc.director.loadScene('game')
    },

    // LIFE-CYCLE CALLBACKS:
    onLoad: function () {
        this.node.on('touchstart', this.playGame, this)
    },
});
