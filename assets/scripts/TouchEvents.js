// WIP: implementação de movimentação via mouse event
cc.Class({
    extends: cc.Component,

    properties: {
        velocity: 50,
        initPos: 7
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {

    },

    start () {

    },

    update: function (dt) {
        this.node.on('touchstart', () => {
            console.log(">> Tocou o dedo da tela -->")
            this.node.setPosition(this.node.position.x  + this.velocity * dt, this.node.position.y)
        }, this)

        
        this.node.on('touchend', () => {
            console.log(">> Soltou o dedo da tela <--")
            this.node.setPosition(this.node.position.x - this.velocity * dt, this.node.position.y )
        }, this)
    },
});
